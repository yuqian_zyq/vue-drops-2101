import { EditorRenderElement, VisualData } from '@/packages/models/editor.model';
import { reactive, readonly } from 'vue';

class VisualDataStore {
  private _state: {data: VisualData};

  constructor(data: VisualData) {
    this._state = reactive({
      data: data
    });
  }

  public get visualData(): VisualData {
    return this._state.data;
  }

  public updateVisual(data: VisualData) {
    this._state.data = data;
  }
}

export const visualDataStore = new VisualDataStore({
  container: {
    width: 800,
    height: 600,
  },
  elements: [] as EditorRenderElement[],
})
