import {
  AbstractControlConfig,
  ElementPropsControlType,
  SelectControlOption,
  SelectControlConfig,
  PickerOptionsControlGroup, PickerOptionsControlConfig,
} from '@/packages/models/editor-operator.model';

export function createInputControl(label: string): AbstractControlConfig {

  return {
    type: ElementPropsControlType.Input,
    label,
  };
}

export function createColorControl(label: string): AbstractControlConfig {

  return {
    type: ElementPropsControlType.Color,
    label,
  };
}

export function createSelectControl(label: string, options: SelectControlOption[]): SelectControlConfig {

  return {
    type: ElementPropsControlType.Select,
    label,
    options,
  };
}

export function createPickerOptionsControl(label: string, groups: PickerOptionsControlGroup[], showKey: string): PickerOptionsControlConfig {

  return {
    type: ElementPropsControlType.PickerOptions,
    label,
    groups,
    showKey,
  };
}

