import { computed, defineComponent, getCurrentInstance, PropType, reactive, ref } from 'vue';
import './visual-editor.scss';
import {
  EditorMarkLines,
  EditorRenderElement,
  MaterialConfig,
  VisualData,
} from '@/packages/models/editor.model';
import { useModel } from './utils/usemodel';
import { EditorMaterial } from '@/packages/components/editor-material';
import { EditorElement } from '@/packages/components/editor-element';
import { editorMaterials } from './config/editor-materials.config';
import { useVisualCommand } from './utils/visual-command';
import { createEvent } from '@/packages/utils/event';
import { $$dialog } from '@/packages/utils/dialog-service';
import { ElButton, ElMessageBox } from 'element-plus';
import { $$dropdown, DropdownOption } from '@/packages/components/dropdown/dropdown-service';
import { EditorOperator } from '@/packages/components/editor-operator';
import router from '@/router';
import { visualDataStore } from '@/store';


export const VisualEditor = defineComponent({
  props: {
    modelValue: {
      type: Object as PropType<VisualData>,
      required: true,
    },
    formData: {
      type: Object as PropType<Record<string, any>>,
      required: true,
    },
  },
  emits: {
    'update:modelValue': (value?: VisualData) => true,
  },
  setup(props) {
    const ctx = getCurrentInstance()!;
    const visualData = useModel(() => props.modelValue, val => ctx.emit('update:modelValue', val));
    const materials = editorMaterials;
    const boardRef = ref({} as HTMLDivElement);
    const focusData = computed(() => {
      const focus: EditorRenderElement[] = [];
      const unFocus: EditorRenderElement[] = [];
      visualData.value.elements.forEach((item) => {
        (item.isFocus ? focus : unFocus).push(item);
      });

      return {focus, unFocus};
    });
    const methods = {
      updateElements: (elements: EditorRenderElement[]) => {
        console.log('更新elements：', elements);
        visualData.value = {
          container: visualData.value.container,
          elements,
        };

      },
      clearFocus: () => {
        visualData.value.elements.forEach((config) => {
          config.isFocus = false;
        });
      },
      showElementData: (el: EditorRenderElement) => {
        $$dialog.textarea(JSON.stringify(el), '节点数据', {editReadonly: true});
      },
      importElementData: async (el: EditorRenderElement) => {
        const text = await $$dialog.textarea('', '输入节点 Json 字符串');
        try {
          const data = JSON.parse(text || '');
          commander.updateElement(data, el);
        } catch (e) {
          console.error(e);
          ElMessageBox.alert('非法的 json 类型');
        }
      },
      togglePreview: () => {
        state.isPreview = !state.isPreview;
      }
    };
    const selectedElementIndex = ref(-1);
    const state = reactive({
      selectedElement: computed(() => {
        return visualData.value.elements[selectedElementIndex.value];
      }),
      editing: true,
      isPreview: false,
    });

    const boardClasses = computed(() => {
      return [
        'editor-board',
        {'edit': state.editing},
      ];
    });

    const boardStyle = computed(() => {
      return {
        width: `${visualData.value.container.width}px`,
        height: `${visualData.value.container.height}px`,
      };
    });

    const dragstartEvent = createEvent();
    const dragendEvent = createEvent();

    const commander = useVisualCommand({
      focusData,
      visualData,
      updateElements: methods.updateElements,
      dragstart: dragstartEvent,
      dragend: dragendEvent,
    });

    const toolBtns = [
      {label: '撤销', icon: 'icon-undo', handler: commander.undo, tip: 'ctrl+z'},
      {label: '重做', icon: 'icon-redo', handler: commander.redo, tip: 'ctrl+y, ctrl+shift+z'},
      {
        label: () => state.editing ? '预览' : '编辑',
        icon: () => state.editing ? 'icon-preview' : 'icon-edit',
        handler: () => {
          state.editing = !state.editing;
          if (!state.editing) {
            methods.clearFocus();
          }
        },
      },
      {
        label: '导入', icon: 'icon-import', handler: async () => {
          const text = await $$dialog.textarea('', '输入导入的 JSON string');
          try {
            visualData.value = JSON.parse(text || '');
          } catch (e) {
            console.error(e);
            ElMessageBox.alert('非法的 json 类型');
          }
        },
      },
      {
        label: '导出', icon: 'icon-export', handler: () => {
          const value = JSON.stringify(visualData.value);
          return $$dialog.textarea(value, '导出 JSON 数据', {editReadonly: true});
        },
      },
      {label: '置顶', icon: 'icon-control-top', handler: commander.placeTop, tip: 'ctrl+up'},
      {label: '置底', icon: 'icon-control-bottom', handler: commander.placeBottom, tip: 'ctrl+down'},
      {label: '删除', icon: 'icon-delete', handler: commander.delete, tip: 'ctrl+d, delete'},
      {label: '清空', icon: 'icon-clear', handler: commander.clear},
      {label: '关闭', icon: 'icon-close', handler: methods.togglePreview},
    ];

    const materialDraggier = (() => {
      let material: MaterialConfig | null = null;
      const boardHandler = {
        dragenter: (event: DragEvent) => {
          event.dataTransfer!.dropEffect = 'move';
        },
        dragover: (event: DragEvent) => {
          // 这里必须阻止默认事件，否在drop不会触发
          event.preventDefault();
        },
        dragleave: (event: DragEvent) => {
          event.dataTransfer!.dropEffect = 'none';
        },
        drop: (event: DragEvent) => {
          event.preventDefault();
          const elements: EditorRenderElement[] = [...visualData.value.elements];
          elements.push({
            top: event.offsetY,
            left: event.offsetX,
            materialKey: material!.key,
            adjustPosition: true,
            isFocus: false,
            zIndex: 0,
            width: 0,
            height: 0,
            isResized: false,
            props: {},
            model: {},
          });
          methods.updateElements(elements);
        },
      };
      const materialHandler = {
        dragstart: (event: DragEvent, config: MaterialConfig) => {
          boardRef.value.addEventListener('dragenter', boardHandler.dragenter);
          boardRef.value.addEventListener('dragover', boardHandler.dragover);
          boardRef.value.addEventListener('dragleave', boardHandler.dragleave);
          boardRef.value.addEventListener('drop', boardHandler.drop);
          material = config;
          dragstartEvent.emit();
        },
        dragend: () => {
          boardRef.value.removeEventListener('dragenter', boardHandler.dragenter);
          boardRef.value.removeEventListener('dragover', boardHandler.dragover);
          boardRef.value.removeEventListener('dragleave', boardHandler.dragleave);
          boardRef.value.removeEventListener('drop', boardHandler.drop);
          material = null;
          dragendEvent.emit();
        },
      };

      return materialHandler;
    })();

    const focusMoveHandler = (() => {
      const markState = reactive({
        x: null as number | null,
        y: null as number | null,
      });
      let dragState: {
        startX: number;
        startY: number;
        startLeft: number;
        startTop: number;
        originPosition: { top: number, left: number }[];
        isDragging: boolean;
        markLines: EditorMarkLines
      };
      const onDragMousemove = (event: MouseEvent) => {
        // 因为用的是mousemove事件监听来移动，所以真正的模拟dragstart事件是在move的时候，并且要加锁来控制防止重复发出事件
        if (!dragState.isDragging) {
          dragState.isDragging = true;
          dragstartEvent.emit();
        }

        let {clientX: moveX, clientY: moveY} = event;
        let {startY, startX} = dragState;

        // 当按住shift时，只能在一个方向上平移
        if (event.shiftKey) {
          if (Math.abs(moveX - startX) > Math.abs(moveY - startY)) {
            moveY = startY;
          } else {
            moveX = startX;
          }
        }

        const currentLeft = dragState.startLeft + moveX - startX;
        const currentTop = dragState.startTop + moveY - startY;
        const currentMark = {
          x: null as number | null,
          y: null as number | null,
        };

        for (const item of dragState.markLines.y) {
          const {top, showTop} = item;
          if (Math.abs(top - currentTop) < 5) {
            moveY = top + startY - dragState.startTop;
            // markState.y = showTop;
            currentMark.y = showTop;
            break;
          }
        }

        for (const item of dragState.markLines.x) {
          const {left, showLeft} = item;
          if (Math.abs(left - currentLeft) < 5) {
            moveX = left + startX - dragState.startLeft;
            // markState.x = showLeft;
            currentMark.x = showLeft;
            break;
          }
        }

        markState.x = currentMark.x;
        markState.y = currentMark.y;
        // console.log(markState);

        const subX = moveX - startX;
        const subY = moveY - startY;
        focusData.value.focus.forEach((item, index) => {
          item.top = dragState.originPosition[index].top + subY;
          item.left = dragState.originPosition[index].left + subX;
        });
      };
      const onDragMouseup = () => {
        document.removeEventListener('mousemove', onDragMousemove);
        document.removeEventListener('mouseup', onDragMouseup);
        if (dragState.isDragging) {
          dragendEvent.emit();
        }
        markState.x = null;
        markState.y = null;
      };
      const executeDrag = (event: MouseEvent) => {
        // console.log(state.selectedElement);
        dragState = {
          startX: event.clientX,
          startY: event.clientY,
          startLeft: state.selectedElement!.left,
          startTop: state.selectedElement!.top,
          originPosition: focusData.value.focus.map(({top, left}) => ({top, left})),
          isDragging: false,
          // markLines 数据y 中的 top指的是元素对齐时的top值，showTop 指的是标线的top值
          markLines: (() => {
            const {focus, unFocus} = focusData.value;
            const {top, left, width, height} = state.selectedElement!;
            let result: EditorMarkLines = {
              x: [
                {
                  left: visualData.value.container.width / 2 - width / 2,
                  showLeft: visualData.value.container.width / 2,
                },
              ],
              y: [
                {
                  top: visualData.value.container.height / 2 - height / 2,
                  showTop: visualData.value.container.height / 2,
                },
              ],
            };

            unFocus.forEach((el) => {
              const {top: t, left: l, width: w, height: h} = el;
              // 顶部对齐顶部
              result.y.push({top: t, showTop: t});
              // 顶部对齐底部
              result.y.push({top: t + h, showTop: t + h});
              // 中间对齐中间
              result.y.push({top: t + h / 2 - height / 2, showTop: t + h / 2});
              // 底部对齐顶部
              result.y.push({top: t - height, showTop: t});
              // 底部对齐底部
              result.y.push({top: t + h - height, showTop: t + h});

              // 左对齐
              result.x.push({left: l, showLeft: l});
              // 左边对齐右边
              result.x.push({left: l + w, showLeft: l + w});
              // 中间对齐中间
              result.x.push({left: l + w / 2 - width / 2, showLeft: l + w / 2});
              // 右边对齐左边
              result.x.push({left: l - width, showLeft: l});
              // 右对齐
              result.x.push({left: l + w - width, showLeft: l + w});
            });

            return result;
          })(),
        };
        document.addEventListener('mousemove', onDragMousemove);
        document.addEventListener('mouseup', onDragMouseup);
      };

      return {
        markState,
        board: {
          handleMousedown: () => {
            return (event: MouseEvent) => {
              if (!event.shiftKey) {
                methods.clearFocus();
                selectedElementIndex.value = -1;
              }
            };
          },
        },
        element: {
          /**
           * TODO: 是否可以设计为：点击事件用来选择，mousedown 事件专门用来处理拖拽
           */
          handleMousedown: (elConfig: EditorRenderElement, index: number) => {
            return (event: MouseEvent) => {
              if (!state.editing) {
                return;
              }

              event.stopPropagation();
              event.preventDefault();

              if (event.shiftKey) {
                if (focusData.value.focus.length <= 1) {
                  elConfig.isFocus = true;
                } else {
                  elConfig.isFocus = !elConfig.isFocus;
                }
                // elConfig.isFocus = !elConfig.isFocus;
              } else {
                if (!elConfig.isFocus) {
                  methods.clearFocus();
                  elConfig.isFocus = true;
                }
              }
              // 拖拽后会改变原来的数据（深拷贝），会导致记录的 selectedElement 在新的数据中找不到。所以还是用 index
              selectedElementIndex.value = index;
              executeDrag(event);
            };
          },
        },
      };
    })();

    /*右键菜单*/
    const contextmenuhandler = {
      editorEl: (e: MouseEvent, el: EditorRenderElement) => {
        if (!state.editing) {
          return;
        }

        e.preventDefault();
        e.stopPropagation();

        $$dropdown({
          reference: e,
          content: () => {
            return (
              <div>
                {/*@ts-ignore*/}
                <DropdownOption label='置顶节点' icon='icon-control-top' {...{onClick: commander.placeTop}} />
                {/*@ts-ignore*/}
                <DropdownOption label='置底节点' icon='icon-control-bottom' {...{onClick: commander.placeBottom}} />
                <DropdownOption label='删除节点' icon='icon-delete' {...{onClick: commander.delete}} />
                <DropdownOption
                  label='查看数据'
                  icon='icon-preview'
                  {...{onClick: (e: MouseEvent) => methods.showElementData(el)}}
                />
                <DropdownOption
                  label='导入节点'
                  icon='icon-import'
                  {...{onClick: (e: MouseEvent) => methods.importElementData(el)}}
                />
              </div>
            );
          },
        });
      },
    };

    const goPreviewPage = () => {
      visualDataStore.updateVisual(visualData.value);
      router.push('/preview');
    };

    return () => {
      return (
        <div class='editor-container'>
          {
            state.isPreview &&
            <div class='editor-preview'>
              <div style='text-align: right'>
                {/*@ts-ignore*/}
                <ElButton onClick={methods.togglePreview}>编辑</ElButton>
              </div>
              <div class='preview-board' style={boardStyle.value}>
                {
                  visualData.value.elements.map((config, index) => (
                    <EditorElement
                      config={config}
                      materials={materials}
                      key={index}
                      formData={props.formData}
                    />
                  ))
                }
              </div>
              <div class='result-data'>{JSON.stringify(props.formData)}</div>
            </div>
          }
          {
            !state.isPreview &&
            <div class='editor-layout'>
              <div class='editor-materials'>
                <div class='material-list'>
                  {materials.configList.map((config, index) => {
                    return (
                      <div
                        class='material-item'
                        key={index}
                        draggable={true}
                        onDragstart={(e) => materialDraggier.dragstart(e, config)}
                        onDragend={materialDraggier.dragend}
                      >
                        <EditorMaterial config={config} />
                      </div>
                    );
                  })}
                </div>
              </div>
              <div class='editor-main'>
                <div class='editor-toolbar'>
                  {/*<div class='page-preview'>*/}
                  {/*  /!*@ts-ignore*!/*/}
                  {/*  <ElButton onClick={goPreviewPage}>去新页面预览</ElButton>*/}
                  {/*</div>*/}
                  <div class='editor-commands'>
                    {toolBtns.map((btn, index) => {
                      const label = typeof btn.label === 'function' ? btn.label() : btn.label;
                      const icon = typeof btn.icon === 'function' ? btn.icon() : btn.icon;
                      const content = (
                        <div class="command-btn" onClick={btn.handler}>
                          <i class={['btn-icon', 'iconfont', icon]} />
                          <div class='title'>{label}</div>
                        </div>
                      );
                      return btn.tip ?
                        <el-tooltip effect="dark" content={btn.tip} placement="top">{content}</el-tooltip> :
                        content;
                    })}
                  </div>
                </div>
                <div class='editor-viewport'>
                  <div class='wrapper'>
                    <div
                      class={boardClasses.value}
                      ref={boardRef}
                      style={boardStyle.value}
                      onMousedown={focusMoveHandler.board.handleMousedown()}
                    >
                      {
                        visualData.value.elements.map((config, index) => (
                          <EditorElement
                            config={config}
                            materials={materials}
                            key={index}
                            formData={props.formData}
                            {...{
                              onMousedown: focusMoveHandler.element.handleMousedown(config, index),
                              onContextmenu: (e: MouseEvent) => contextmenuhandler.editorEl(e, config),
                            }}
                          />
                        ))
                      }

                      {
                        focusMoveHandler.markState.x != null &&
                        <div class="board-mark-line-x" style={{left: `${focusMoveHandler.markState.x}px`}} />
                      }
                      {
                        focusMoveHandler.markState.y != null &&
                        <div class="board-mark-line-y" style={{top: `${focusMoveHandler.markState.y}px`}} />
                      }
                    </div>
                  </div>
                </div>
              </div>

              <div class='editor-operator'>
                <EditorOperator
                  editorElement={state.selectedElement as EditorRenderElement}
                  materials={materials}
                  visualData={visualData.value}
                  updateElement={commander.updateElement}
                  updateBoardContainer={commander.updateBoardContainer}
                />
              </div>
            </div>
          }
        </div>
      );
    };
  },
});
