import { PickerOptionsControlConfig } from '@/packages/models/editor-operator.model';
import { createApp, defineComponent, getCurrentInstance, onMounted, PropType, reactive } from 'vue';
import { defer } from '@/packages/utils/defer';
import { ElButton, ElDialog, ElInput, ElTable, ElTableColumn } from 'element-plus';
import deepcopy from 'deepcopy';

export interface PickerOptionsEditOption {
  data: any[];
  config: PickerOptionsControlConfig;
  onConfirm: (val: any[]) => void;
}

const ServiceComponent = defineComponent({
  props: {
    option: {
      type: Object as PropType<PickerOptionsEditOption>,
      required: true
    },
  },
  setup(props) {
    const ctx = getCurrentInstance()!;

    const state = reactive({
      option: props.option,
      showFlag: false,
      mounted: (() => {
        const dfd = defer();
        onMounted(() => setTimeout(() => {
          dfd.resolve();
        }, 0));

        return dfd.promise;
      })(),
      editData: [] as any[]
    });

    const methods = {
      service: (option: PickerOptionsEditOption) => {
        state.option = option;
        state.editData = deepcopy(option.data || []);
        methods.show();
      },
      show: async () => {
        await state.mounted;
        state.showFlag = true;
      },
      hide: () => {
        state.showFlag = false;
      },
      add: () => {
        state.editData.push({})
      },
      reset: () => {
        state.editData = deepcopy(state.option.data)
      },
    }

    const handler = {
      onConfirm: () => {
        state.option.onConfirm(state.editData);
        methods.hide();
      },
      onCancel: () => {
        methods.hide();
      }
    }

    Object.assign(ctx.proxy, methods);

    return () => {
      return (
          // @ts-ignore
          <ElDialog v-model={state.showFlag}>
            {{
              default: () => (
                <div>
                  <div>
                    {/*@ts-ignore*/}
                    <ElButton onClick={methods.add}>添加</ElButton>
                    {/*@ts-ignore*/}
                    <ElButton onClick={methods.reset}>重置</ElButton>
                  </div>
                  <ElTable data={state.editData}>
                    {/*@ts-ignore*/}
                    <ElTableColumn type='index' />
                    {state.option.config.groups.map((item, index) => {
                      return (
                        // @ts-ignore
                        <ElTableColumn label={item.label}>
                          {{
                            default: ({row}: {row: any}) => (
                              <ElInput v-model={row[item.field]} />
                            )
                          }}
                        </ElTableColumn>
                      )
                    })}
                    {/*@ts-ignore*/}
                    <ElTableColumn label='操作栏'>
                      <ElButton type='danger'>删除</ElButton>
                    </ElTableColumn>
                  </ElTable>
                </div>
              ),
              footer: () => (
                <>
                  {/*@ts-ignore*/}
                  <ElButton onClick={handler.onCancel}>取消</ElButton>
                  {/*@ts-ignore*/}
                  <ElButton type='primary' onClick={handler.onConfirm}>确定</ElButton>
                </>
              )
            }}
          </ElDialog>
      );
    }
  }
})

export const $$PickerOptionsEdit = (() => {
  let instance: any;

  return (option: Omit<PickerOptionsEditOption, 'onConfirm'>) => {
    if (!instance) {
      // 当实例不存在时去动态创建
      const el = document.createElement('div');
      document.body.appendChild(el);
      const app = createApp(ServiceComponent, {option});
      instance = app.mount(el);
    }
    const dfd = defer<any[]>();
    instance.service({
      ...option,
      onConfirm: dfd.resolve
    });

    return dfd.promise;
  }
})();
