import { defineComponent, PropType } from 'vue';
import './picker-options-control.scss';
import { AbstractControlConfig, PickerOptionsControlConfig } from '@/packages/models/editor-operator.model';
import { useModel } from '@/packages/utils/usemodel';
import { ElButton, ElTag } from 'element-plus';
import { $$PickerOptionsEdit } from '@/packages/components/picker-options-control/picker-options-edit.service';

export const PickerOptionsControl = defineComponent({
  props: {
    modelValue: {
      type: Array as PropType<any[]>,
    },
    controlConfig: {
      type: Object as PropType<PickerOptionsControlConfig>,
      required: true
    }
  },
  emits: {
    'update:modelValue': (val?: any[]) => true,
  },
  setup: (props, ctx) => {
    const modelValue = useModel(
      () => props.modelValue,
      val => ctx.emit('update:modelValue', val)
    )

    const clickHandler = async () => {
      const data = await $$PickerOptionsEdit({
        config: props.controlConfig,
        data: props.modelValue || []
      });
      modelValue.value = data;
    }

    return () => {
      return (
        <div>
          {
            (!modelValue.value || modelValue.value.length === 0) ?
              /*@ts-ignore*/
              <ElButton onClick={clickHandler}>添加选项</ElButton>:
              <div class='tag-list'>
                {modelValue.value.map(item => {
                  return <ElTag>{item[props.controlConfig.showKey]}</ElTag>
                })}
              </div>
          }
        </div>
      )
    }
  },
})
