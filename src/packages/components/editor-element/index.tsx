import { defineComponent, PropType, ref, Ref, onMounted } from 'vue';
import { EditorRenderElement } from '@/packages/models/editor.model';
import { EditorMaterials } from '@/packages/models/editor-materials';

/**
 * 编辑器页面中的元素渲染组件
 */
export const EditorElement = defineComponent({
  props: {
    config: {
      type: Object as PropType<EditorRenderElement>,
      required: true,
    },
    materials: {
      type: Object as PropType<EditorMaterials>,
      required: true,
    },
    formData: {
      type: Object as PropType<Record<string, any>>,
      required: true,
    },
  },
  setup(props) {
    const elementRef: Ref<unknown> = ref(null);

    onMounted(() => {
      const elementEl = elementRef.value as HTMLElement;
      const {offsetWidth, offsetHeight} = elementEl;
      const config = props.config;
      if (config.adjustPosition) {
        config.top = props.config.top - offsetHeight / 2;
        config.left = props.config.left - offsetWidth / 2;
        config.height = offsetHeight;
        config.width = offsetWidth;
        config.adjustPosition = false;
      }
    });

    return () => {
      const {config, materials} = props;
      const elStyle = {
        top: `${props.config.top}px`,
        left: `${props.config.left}px`,
        zIndex: props.config.zIndex,
      };
      const elComponent = materials.configMap[config.materialKey];
      const formData = props.formData;
      const Render = elComponent.element({
        props: props.config.props || {},
        model: Object.entries(props.config.model || {})
          .reduce((prev, [propName, modelName]) => {
            prev[propName] = {
              [propName === 'default' ? 'modelValue' : propName]: formData[modelName],
              [propName === 'default' ? 'onUpdate:modelValue' : 'onChange']: (val: any) => formData[modelName] = val,
            };
            console.log(prev);
            return prev;
          }, {}),
      });

      return (
        <div class={['editor-element', {'editor-element-focus': props.config.isFocus}]}
          style={elStyle}
          ref={elementRef}>
          {Render}
        </div>
      );
    };
  },
});
