import {
  computed,
  createApp,
  defineComponent,
  getCurrentInstance,
  onMounted,
  PropType,
  reactive,
  onBeforeUnmount,
  ref, Ref, provide, inject,
} from 'vue';
import './dropdown.scss';
import { defer } from '@/packages/utils/defer';

interface DropdownServiceOption {
  reference: MouseEvent | HTMLElement;
  content: () => JSX.Element;
}

const DropdownServiceProvider = (() => {
  const DROPDOWN_SERVICE_PROVIDER = '@@DROPDOWN_SERVICE_PROVIDER';

  return {
    provide: (handler: {onClick: () => void}) => {
      provide(DROPDOWN_SERVICE_PROVIDER, handler);
    },
    inject: () => {
      return inject(DROPDOWN_SERVICE_PROVIDER) as {onClick: () => void};
    }
  }
})()

const ServiceComponent = defineComponent({
  props: {
    option: {
      type: Object as PropType<DropdownServiceOption>,
      required: true
    }
  },
  setup(props) {
    const ctx = getCurrentInstance()!;
    const dropdownEl: Ref<HTMLElement> = ref({} as HTMLElement);

    const state = reactive({
      option: props.option,
      showFlag: false,
      top: 0,
      left: 0,
      mounted: (() => {
        const dfd = defer();
        onMounted(() => {
          setTimeout(() => {
            dfd.resolve();
          }, 0);
        });

        return dfd.promise;
      })()
    });

    const methods = {
      show: async () => {
        await state.mounted;
        state.showFlag = true;
      },
      hide: () => {
        state.showFlag = false;
      }
    }

    const service = (option: DropdownServiceOption) => {
      state.option = option;

      if ('addEventListener' in option.reference) {
        const {top, left, height} = option.reference.getBoundingClientRect()!;
        state.top = top + height;
        state.left = left;
      } else {
        const {clientY, clientX} = option.reference;
        state.left = clientX;
        state.top = clientY;
      }

      // state.showFlag = true;
      methods.show();
    }

    const classes = computed(() => {
      return [
        'dropdown',
        {'dropdown-activated': state.showFlag}
      ]
    });

    const styles = computed(() => {
      return {
        top: `${state.top}px`,
        left: `${state.left}px`,
      }
    });

    Object.assign(ctx.proxy, {service});

    const onMousedownDocument  = (e: MouseEvent) => {
      if (dropdownEl.value.contains(e.target as HTMLElement)) {
        return;
      }
      // state.showFlag = false;
      methods.hide();
    }

    onMounted(() => {
      document.body.addEventListener('mousedown', onMousedownDocument, true)
    });
    onBeforeUnmount(() => {
      document.body.removeEventListener('mousedown', onMousedownDocument, true);
    });

    DropdownServiceProvider.provide({
      onClick: () => {
        methods.hide();
      }
    })

    return () => {
      return (
        <div class={classes.value} style={styles.value} ref={dropdownEl}>
          {state.option.content()}
        </div>
      )
    }
  }
});

export const DropdownOption = defineComponent({
  props: {
    label: {type: String},
    icon: {type: String}
  },
  emits: {
    click: (e: MouseEvent) => true
  },
  setup(props, ctx) {
    const {onClick: dropdownClickHandler} = DropdownServiceProvider.inject();
    const handler = {
      onClick: (e: MouseEvent) => {
        ctx.emit('click', e);
        dropdownClickHandler();
      }
    }

    return () => {
      return (
        <div class='dropdown-option' onClick={handler.onClick}>
          <i class={`iconfont ${props.icon}`} />
          <span>{props.label}</span>
        </div>
      );
    }
  }
});

export const $$dropdown = (() => {
  let instance: any;

  return (option: DropdownServiceOption) => {
    if (!instance) {
      // 当实例不存在时去动态创建
      const el = document.createElement('div');
      document.body.appendChild(el);
      const app = createApp(ServiceComponent, {option});
      // app.component('dropdown-option', DropdownOption);
      instance = app.mount(el);
    }

    instance.service(option);
  }
})();
