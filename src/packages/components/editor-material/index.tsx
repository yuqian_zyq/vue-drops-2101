import { defineComponent, PropType } from 'vue';
import './editor-material.scss';
import { MaterialConfig } from '@/packages/models/editor.model';

/**
 * 编辑器物料组件
 */
export const EditorMaterial = defineComponent({
  props: {
    config: {
      type: Object as PropType<MaterialConfig>,
      required: true
    }
  },
  setup(props) {
    return () => (
      <div class='editor-material'>
        <div class='material-label'>{props.config.name}</div>
        <div class="material-element">
          {props.config.preview()}
        </div>
      </div>
    );
  }
});
