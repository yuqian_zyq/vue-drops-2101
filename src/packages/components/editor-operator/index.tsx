import { defineComponent, PropType, reactive, watch } from 'vue';
import './editor-operator.scss';
import {
  EditorBoardContainer,
  EditorRenderElement,
  VisualData,
} from '@/packages/models/editor.model';
import { EditorMaterials } from '@/packages/models/editor-materials';
import { ElButton, ElColorPicker, ElForm, ElFormItem, ElInput, ElInputNumber, ElOption, ElSelect } from 'element-plus';
import deepcopy from 'deepcopy';
import {
  ElementPropsControlType,
  PickerOptionsControlConfig,
  SelectControlConfig,
} from '@/packages/models/editor-operator.model';
import { PickerOptionsControl } from '@/packages/components/picker-options-control';

export const EditorOperator = defineComponent({
  props: {
    editorElement: {
      type: Object as PropType<EditorRenderElement>,
      // required: true,
    },
    materials: {
      type: Object as PropType<EditorMaterials>,
      required: true,
    },
    visualData: {
      type: Object as PropType<VisualData>,
      required: true,
    },
    updateElement: {
      type: Function as PropType<(changed: EditorRenderElement, origin: EditorRenderElement) => void>,
      required: true,
    },
    updateBoardContainer: {
      type: Function as PropType<(value: EditorBoardContainer) => void>,
      required: true,
    },
  },
  setup(props) {
    const state = reactive({
      editData: {} as EditorBoardContainer | EditorRenderElement,
    });

    const methods = {
      apply: () => {
        if (!props.editorElement) {
          const {width, height} = state.editData as EditorBoardContainer;
          props.updateBoardContainer({width, height});
        } else {
          const changed = state.editData as EditorRenderElement;
          props.updateElement(changed, props.editorElement);
        }
      },
      reset: () => {
        if (!props.editorElement) {
          state.editData = deepcopy(props.visualData.container);
        } else {
          state.editData = deepcopy(props.editorElement);
        }
      },
    };

    watch(() => props.editorElement, (el) => {
      methods.reset();
    }, {immediate: true});

    return () => {
      let content: JSX.Element | undefined;
      let modelContent: JSX.Element | undefined;

      if (!props.editorElement) {
        content = (
          <div class='container-form-content'>
            <ElFormItem label='容器宽度'>
              {/*@ts-ignore*/}
              <ElInputNumber v-model={state.editData.width} step={100} />
            </ElFormItem>
            <ElFormItem label='容器高度'>
              {/*@ts-ignore*/}
              <ElInputNumber v-model={state.editData.height} step={100} />
            </ElFormItem>
          </div>
        );
      } else {
        const {materialKey} = props.editorElement;
        const element = props.materials.configMap[materialKey];

        if (element && element.props) {
          content = (
            <div class='element-form-content'>
              {
                Object.entries(element.props).map(([propName, propConfig]) => {
                  return (
                    <ElFormItem label={propConfig.label}>
                      {(() => {
                        let result: JSX.Element | undefined;
                        switch (propConfig.type) {
                          case ElementPropsControlType.Input:
                            result = <ElInput v-model={(state.editData as EditorRenderElement).props[propName]} />;
                            break;
                          case ElementPropsControlType.Color:
                            result = <ElColorPicker v-model={(state.editData as EditorRenderElement).props[propName]} />;
                            break;
                          case ElementPropsControlType.Select:
                            result =
                              <ElSelect v-model={(state.editData as EditorRenderElement).props[propName]}>
                                {(propConfig as SelectControlConfig).options.map((option) => {
                                  return <ElOption label={option.label} value={option.value} key={option.value} />;
                                })}
                              </ElSelect>;
                            break;
                          case ElementPropsControlType.PickerOptions:
                            result =
                              <div>
                                <PickerOptionsControl
                                  v-model={(state.editData as EditorRenderElement).props[propName]}
                                  controlConfig={propConfig as PickerOptionsControlConfig}
                                />
                              </div>;
                            break;
                        }

                        return result;
                      })()}
                    </ElFormItem>
                  );
                })
              }
            </div>
          );
        }

        if (element && element.model) {
          modelContent = (
            <div>
              {
                Object.entries(element.model).map(([modelName, label]) => {
                  return (
                    <ElFormItem label={label}>
                      <ElInput v-model={(state.editData as EditorRenderElement).model[modelName]} />
                    </ElFormItem>
                  );
                })
              }
            </div>
          );
        }
      }

      return (
        <div class='editor-operator-content'>
          <ElForm labelPosition='top'>
            {content}
            {modelContent}
            <ElFormItem>
              {/*@ts-ignore*/}
              <ElButton type='primary' onClick={methods.apply}>应用</ElButton>
              {/*@ts-ignore*/}
              <ElButton onClick={methods.reset}>重置</ElButton>
            </ElFormItem>
          </ElForm>
        </div>
      );
    };
  },
});
