import { defineComponent, PropType } from 'vue';
import './editor-preview.scss';
import { VisualData } from '@/packages/models/editor.model';
import { EditorElement } from '@/packages/components/editor-element';

export const EditorPreview = defineComponent({
  props: {
    visualData: {
      type: Object as PropType<VisualData>,
      required: true,
    },
  },
  setup(props) {


    return () => {
      return (
        <div class='editor-preview'>
        </div>
      )
    }
  }
});
