export interface Command {
  name: string;                                               // 唯一标识
  keyboard?: string | string[];                               // 快捷键
  execute: (...args: any[]) => CommandExecutor;           // 命令执行器
  followQueue?: boolean;                                      // 是否需要将命令执行后的执行器对象缓存队列
  init?: () => ((() => void) | undefined);                    // 命令初始化函数
  data?: any,
}

/**
 * 命令执行器对象
 */
export interface CommandExecutor {
  undo?: () => void; // 撤回命令的操作
  redo: () => void; // 重做命令的操作
}
