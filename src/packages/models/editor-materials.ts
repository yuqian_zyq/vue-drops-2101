import { MaterialBase, MaterialConfig } from '@/packages/models/editor.model';
import { AbstractControlConfig } from '@/packages/models/editor-operator.model';

export class EditorMaterials {
  configMap: { [key: string]: MaterialConfig } = {};

  constructor() {
    // if (data) {
    //   Object.entries(data).forEach(([key, config]) => {
    //     this.configMap[key] = {...config, key };
    //   })
    // }
  }

  get configList(): MaterialConfig[] {
    return Object.values(this.configMap);
  }

  /**
   * 注册物料
   * @param key
   * @param config
   */
  public registryMaterial<T extends Record<string, AbstractControlConfig>, U extends Record<string, string>>(
    key: string, config: MaterialBase<T, U>,
  ): void {
    this.configMap[key] = {...config, key} as MaterialConfig;
  }
}
