/**
 * 可编辑元素属性的控件类型
 */
export enum ElementPropsControlType {
  Input = 'input',
  Color = 'color',
  Select = 'select',
  PickerOptions = 'pickerOptions'
}

/**
 * 属性控件描述的基本数据
 */
export interface AbstractControlConfig {
  type: ElementPropsControlType;
  label: string;
}

/**
 * 下拉选择选项数据
 */
export interface SelectControlOption {
  label: string;
  value: string;
}

/**
 * select 类型的props描述
 */
export interface SelectControlConfig extends AbstractControlConfig {
  options: SelectControlOption[];
}


export type PickerOptionsControlGroup = {
  label: string;
  field: string;
};

/**
 * 选择类型的元素下拉选项配置控件
 */
export interface PickerOptionsControlConfig extends AbstractControlConfig {
  groups: PickerOptionsControlGroup[];
  showKey: string;
}
