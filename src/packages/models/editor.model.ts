import { AbstractControlConfig } from '@/packages/models/editor-operator.model';

/**
 * 编辑器最终数据
 */
export interface VisualData {
  container: EditorBoardContainer;
  elements: EditorRenderElement[];
}

export interface EditorBoardContainer {
  width: number;
  height: number;
}

/**
 * 编辑器画板中所渲染的元素单元
 */
export interface EditorRenderElement {
  top: number;
  left: number;
  // render: () => JSX.Element; //  不能直接用函数，给为用 materialKey 去查找，因为，最终数据JSON会转为字符串
  materialKey: string;
  adjustPosition?: boolean; // 是否需要调整位置
  isFocus: boolean;
  zIndex: number;
  width: number;
  height: number;
  isResized: boolean;
  props: Record<string, any>; // 编辑元素的属性对象
  model: Record<string, string>; // 元素绑定字段
}

/**
 * 物料数据中 element 元素 函数的参数类型
 */
export interface MaterialElementParams<T extends Record<string, AbstractControlConfig>, U extends Record<string, string> = {}> {
  props: { [k in keyof T]: any },
  model: Partial<{
    [key in keyof U]: any
  }>
}

/**
 * 物料元素配置
 */
export interface MaterialConfig extends MaterialBase<Record<string, AbstractControlConfig>, Record<string, string>> {
  key: string;
}

export interface MaterialBase<T extends Record<string, AbstractControlConfig>, U extends Record<string, string>> {
  name: string; // 物料的名字
  preview: () => JSX.Element; // 预览组件
  element: (data: MaterialElementParams<T, U>) => JSX.Element; // 要渲染的组件
  props?: T; // 无聊中对于元素的属性的一些配置
  model?: U;
}

/**
 * 对齐辅助线
 */
export interface EditorMarkLines {
  x: { left: number, showLeft: number }[],
  y: { top: number, showTop: number }[]
}
