import { defineComponent, PropType, reactive, createApp, getCurrentInstance } from 'vue';
import { ElInput, ElDialog, ElButton } from 'element-plus';
import { defer } from '@/packages/utils/defer';

enum DialogServiceEditType {
  Textarea = 'Textarea',
  Input = 'Input',
}

interface DialogServiceOption {
  title?: string;
  editType: DialogServiceEditType;
  editReadonly?: boolean;
  editValue?: string;
  onConfirm: (val?: string) => void;
}

/**
 * 生成弹窗唯一标识
 */
const keyGenerator = (() => {
  let count = 0;
  return () => `auto_key_${count++}`;
})();

const ServiceComponent = defineComponent({
  props: {
    option: {type: Object as PropType<DialogServiceOption>, required: true},
  },
  setup(props) {
    const ctx = getCurrentInstance()!;
    /**
     * 创建响应式的状态，动态创建组建后可以响应数据的变化
     */
    const state = reactive({
      option: props.option,
      editValue: '',
      showFlag: false,
      key: keyGenerator(),
    });
    const methods = {
      service: (option: DialogServiceOption) => {
        state.option = option;
        state.editValue = option.editValue || '';
        state.key = keyGenerator();
        methods.show();
      },
      show: () => {
        state.showFlag = true;
      },
      hide: () => state.showFlag = false,
    };

    const handler = {
      onConfirm: () => {
        state.option.onConfirm(state.editValue);
        methods.hide();
      },
      onCancel: () => {
        methods.hide();
      },
    };

    /* 使用proxy代理methods，将methods挂载到当前实例，可以通过组件实例来访问 */
    Object.assign(ctx.proxy, methods);

    return () => (
      // @ts-ignore
      <ElDialog v-model={state.showFlag} title={state.option.title} key={state.key}>
        {{
          default: () => (
            <div>
              {
                state.option.editType === DialogServiceEditType.Textarea ?
                  <ElInput type="textarea" {...{rows: 10}} v-model={state.editValue} /> :
                  <ElInput v-model={state.editValue} />
              }
            </div>
          ),
          footer: () => (
            <div>
              <ElButton {...{onClick: handler.onCancel} as any}>取消</ElButton>
              <ElButton {...{onClick: handler.onConfirm} as any}>确定</ElButton>
            </div>
          ),
        }}
      </ElDialog>
    );
  },
});

const DialogService = (() => {
  let instance: any;

  return (option: DialogServiceOption) => {
    /* 动态挂载 */
    if (!instance) {
      const el = document.createElement('div');
      document.body.appendChild(el);
      const app = createApp(ServiceComponent, {option});
      instance = app.mount(el);
    }
    instance.service(option);
  };
})();

export const $$dialog = Object.assign(DialogService, {
  /**
   * 提供一个静态的 input 方法
   */
  input: (initValue?: string, title?: string, option?: Omit<DialogServiceOption, 'editType' | 'onConfirm'>) => {
    const dfd = defer<string | undefined>();
    const opt: DialogServiceOption = {
      ...option,
      editType: DialogServiceEditType.Input,
      onConfirm: dfd.resolve,
      editValue: initValue,
      title,
    };
    DialogService(opt);

    return dfd.promise;
  },
  /**
   * 提供一个静态的 textarea 方法
   */
  textarea: (initValue?: string, title?: string, option?: Omit<DialogServiceOption, 'editType' | 'onConfirm'>) => {
    const dfd = defer<string | undefined>();
    const opt: DialogServiceOption = {
      ...option,
      editType: DialogServiceEditType.Textarea,
      onConfirm: dfd.resolve,
      editValue: initValue,
      title,
    };
    DialogService(opt);

    return dfd.promise;
  },
});
