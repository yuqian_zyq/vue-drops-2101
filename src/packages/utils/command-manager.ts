import { reactive, onUnmounted } from 'vue';
import { Command, CommandExecutor } from '@/packages/models/command.model';
import { KeyboardCode } from '@/packages/models/keyboard-code';

/**
 * 命令管理对象
 * 思路大概是：每次命令操作存储的是执行器对象，而不是所有元素的状态
 */
// export class CommandManager {
//   // 命令队列，每次执行命令后将返回的命令执行器对象存储到该队列
//   queue: CommandExecutor[] = [];
//
//   // 当前命令队列的索引指针，每次执行 undo。redo 都会动态变化
//   current = 0;
//
//   commands: Record<string, (...args: unknown[]) => void> = {}
//
//   constructor() {
//     //
//   }
//
//   /**
//    * 注册命令
//    */
//   registry(command: Command) {
//     this.commands[command.name] = (...args) => {
//       command.execute(...args);
//     }
//   }
// }

export function useCommander() {
  const state = reactive({
    current: -1,
    commands: {} as Record<string, (...args: unknown[]) => void>,
    queue: [] as CommandExecutor[],
    commandArr: [] as Command[],
    destroyList: [] as ((() => void) | undefined)[],
  });

  const registry = (command: Command) => {
    state.commandArr.push(command);
    state.commands[command.name] = (...args) => {
      const {undo, redo} = command.execute(...args);
      if (command.followQueue !== false) {
        // state.queue.push({undo, redo});
        // state.current += 1;

        let {queue, current} = state;
        if (queue.length > 0) {
          queue = queue.slice(0, current + 1);
          state.queue = queue;
        }
        state.queue.push({undo, redo});
        state.current = current + 1;
      }
      redo();
    };
  };

  const keyboardEvent = (() => {
    const onKeydown = (event: KeyboardEvent) => {
      // 事件拦截，像 input focus时，此时一些快捷键是针对于input的
      if (document.activeElement !== document.body) {
        return;
      }
      const {keyCode, shiftKey, altKey, ctrlKey, metaKey} = event;
      const keyList: string[] = [];

      if (ctrlKey || metaKey) keyList.push('ctrl');
      if (shiftKey) keyList.push('shift');
      if (altKey) keyList.push('alt');
      keyList.push(KeyboardCode[keyCode]);
      const keyName = keyList.join('+');

      state.commandArr.forEach(({keyboard, name}) => {
        if (!keyboard) {
          return;
        }
        const keys = Array.isArray(keyboard) ? keyboard : [keyboard];
        if (keys.includes(keyName)) {
          state.commands[name]();
          // 可以阻止浏览器自己定义的快捷键行为
          event.stopPropagation();
          event.preventDefault();
        }
      })
    }

    return () => {
      window.addEventListener('keydown', onKeydown);

      return () => {
        window.removeEventListener('keydown', onKeydown);
      };
    };
  })()

  const init = () => {
    const onkeydown = (e: KeyboardEvent) => {
      // console.log('监听到键盘事件');
    };
    window.addEventListener('keydown', onkeydown);
    state.commandArr.forEach((command) => {
      !!command.init && state.destroyList.push(command.init());
    });
    state.destroyList.push(keyboardEvent());
    state.destroyList.push(() => {
      return window.removeEventListener('keydown', onkeydown);
    });
  };

  registry({
    name: 'undo',
    keyboard: 'ctrl+z',
    followQueue: false,
    execute: () => {
      return {
        redo: () => {
          console.log('执行撤销');
          let {current} = state;
          if (current === -1) {
            return;
          }
          const item = state.queue[current];
          if (item) {
            item.undo && item.undo();
            state.current--;
          }
        },
      };
    },
  });

  registry({
    name: 'redo',
    keyboard: ['ctrl+y', 'ctrl+shift+z'],
    followQueue: false,
    execute: () => {
      return {
        redo: () => {
          console.log('执行重做');
          let {current} = state;
          const item = state.queue[current + 1];
          if (item) {
            item.redo();
            state.current += 1;
          }
        },
      };
    },
  });

  onUnmounted(() => {
    state.destroyList.forEach((fn) => !!fn && fn());
  });

  return {
    state,
    registry,
    init
  };
}
