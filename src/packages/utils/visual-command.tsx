import { EditorBoardContainer, EditorRenderElement, VisualData } from '@/packages/models/editor.model';
import { useCommander } from './command-manager';
import deepcopy from 'deepcopy';

interface VisualCommandParams {
  focusData: {
    value: {
      focus: EditorRenderElement[],
      unFocus: EditorRenderElement[],
    }
  };
  updateElements: (value: EditorRenderElement[]) => void;
  visualData: { value: VisualData };
  dragstart: {
    on: (cb: () => void) => void,
    off: (cb: () => void) => void
  },
  dragend: {
    on: (cb: () => void) => void,
    off: (cb: () => void) => void
  }
}

export function useVisualCommand(
  {focusData, updateElements, visualData, dragstart, dragend}: VisualCommandParams,
) {
  const commander = useCommander();

  commander.registry({
    name: 'delete',
    keyboard: ['delete', 'ctrl+d'],
    followQueue: true,
    execute: () => {
      console.log('执行删除');
      const data = {
        before: visualData.value.elements || [],
        after: focusData.value.unFocus || [],
      };

      return {
        undo: () => {
          updateElements(deepcopy(data.before));
        },
        redo: () => {
          updateElements(deepcopy(data.after));
        },
      };
    },
  });

  commander.registry({
    name: 'drag',
    followQueue: true,
    init() {
      this.data = {
        before: [] as EditorRenderElement[],
      };
      const handler = {
        dragstart: () => this.data.before = deepcopy(visualData.value.elements || []),
        dragend: () => commander.state.commands.drag(),
      };
      dragstart.on(handler.dragstart);
      dragend.on(handler.dragend);

      return () => {
        dragstart.off(handler.dragstart);
        dragend.off(handler.dragend);
      };
    },
    execute() {
      let before = this.data.before;
      let after = deepcopy(visualData.value.elements || []);

      /**
       * 这个方法里面的 deepcopy 的使用很关键
       * 因为返回的函数执行 updateElements 时会读取 after 和 before 变量，而且都是对象类型，
       * 如果不进行深拷贝，多次执行后，就会出现修改的是同一个对象而出现问题
       */
      return {
        redo: () => {
          updateElements(deepcopy(after));
        },
        undo: () => {
          updateElements(deepcopy(before));
        },
      };
    },
  });

  commander.registry({
    name: 'clear',
    // keyboard: ['delete', 'ctrl+d'],
    followQueue: true,
    execute: () => {
      console.log('执行清空');
      const data = {
        before: deepcopy(visualData.value.elements),
        after: deepcopy([]),
      };

      return {
        undo: () => {
          updateElements(deepcopy(data.before));
        },
        redo: () => {
          updateElements(deepcopy(data.after));
        },
      };
    },
  });

  commander.registry({
    name: 'placeTop',
    keyboard: ['ctrl+up'],
    execute: () => {
      console.log('执行 placeTop');
      const data = {
        before: deepcopy(visualData.value.elements),
        after: deepcopy((() => {
          const {focus, unFocus} = focusData.value;
          const maxZIndex = unFocus.reduce((acc, cur) => {
            return Math.max(acc, cur.zIndex);
          }, 0);
          focus.forEach((el) => el.zIndex = maxZIndex + 1);
          return deepcopy(visualData.value.elements);
        })()),
      };
      return {
        redo: () => {
          updateElements(deepcopy(data.after));
        },
        undo: () => {
          updateElements(deepcopy(data.before));
        },
      };
    },
  });

  commander.registry({
    name: 'placeBottom',
    keyboard: ['ctrl+down'],
    execute: () => {
      console.log('执行 placeBottom');
      const data = {
        before: deepcopy(visualData.value.elements),
        after: deepcopy((() => {
          const {focus, unFocus} = focusData.value;
          const unFocusMinZIndex = unFocus.reduce((acc, cur) => {
            return Math.min(acc, cur.zIndex);
          }, Infinity);
          let index = unFocusMinZIndex - 1;
          if (index < 0) {
            unFocus.forEach((el) => el.zIndex += 1);
            index = 0;
          }
          focus.forEach((el) => el.zIndex = index);
          return deepcopy(visualData.value.elements);
        })()),
      };
      return {
        redo: () => {
          updateElements(deepcopy(data.after));
        },
        undo: () => {
          updateElements(deepcopy(data.before));
        },
      };
    },
  });

  commander.registry({
    name: 'updateElement',
    execute: (newEl: EditorRenderElement, oldEl: EditorRenderElement) => {
      console.log('执行单元素更新：');
      console.log('新 =>', newEl);
      console.log('旧 =>', oldEl);
      const els = deepcopy(visualData.value.elements || []);
      let data = {
        before: deepcopy(visualData.value.elements),
        after: (() => {
          const index = visualData.value.elements.indexOf(oldEl);
          if (index > -1) {
            els.splice(index, 1, newEl);
          }
          return deepcopy(els);
        })(),
      };
      return {
        redo: () => {
          updateElements(deepcopy(data.after));
        },
        undo: () => {
          updateElements(deepcopy(data.before));
        },
      };
    },
  });

  commander.registry({
    name: 'updateBoardContainer',
    execute: (value: EditorBoardContainer) => {
      console.log('执行容器更新：', value);
      const data = {
        before: deepcopy(visualData.value.container),
        after: deepcopy({...visualData.value.container, ...value})
      }

      return {
        redo: () => {
          visualData.value = {...visualData.value, container: {...data.after}};
        },
        undo: () => {
          visualData.value = {...visualData.value, container: {...data.before}};
        }
      }
    }
  })

  commander.init();

  return {
    undo: () => commander.state.commands.undo(),
    redo: () => commander.state.commands.redo(),
    delete: () => commander.state.commands.delete(),
    clear: () => commander.state.commands.clear(),
    placeTop: () => commander.state.commands.placeTop(),
    placeBottom: () => commander.state.commands.placeBottom(),
    updateElement: (newEl: EditorRenderElement, oldEl: EditorRenderElement) =>
      commander.state.commands.updateElement(newEl, oldEl),
    updateBoardContainer: (value: EditorBoardContainer) => commander.state.commands.updateBoardContainer(value)
  };
}
