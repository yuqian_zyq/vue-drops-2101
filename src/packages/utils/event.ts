type Listener = () => void;

export function createEvent() {
  const listeners: Listener[] = [];

  return {
    // 注册事件
    on: (cb: Listener) => {
      listeners.push(cb);
    },
    // 移除事件
    off: (cb: Listener) => {
      const index = listeners.indexOf(cb);
      if (index > -1) {
        listeners.splice(index, 1);
      }
    },
    // 触发事件
    emit: (...args: Parameters<Listener>) => {
      listeners.forEach(item => item(...args));
    }
  }
}
