import { ElButton, ElInput, ElOption, ElSelect } from 'element-plus';
import {
  createColorControl,
  createPickerOptionsControl,
  createInputControl,
  createSelectControl,
} from '@/packages/visual-editor-props';
import { EditorMaterials } from '@/packages/models/editor-materials';


export const editorMaterials = new EditorMaterials();

// 这里还是要调用方法来注册，不用静态config直接生成，主要是为了读取 props 的 key，方便类型提示
editorMaterials.registryMaterial('text', {
  name: '文本',
  preview: () => '普通文本',
  element: ({props}) => {
    const style = {
      color: props.color,
      fontSize: props.size,
    };
    return <span style={style}>{props.text || '普通文本'}</span>;
  },
  props: {
    text: createInputControl('显示本文'),
    color: createColorControl('字体颜色'),
    size: createSelectControl('字体大小', [
      {label: '14px', value: '14px'},
      {label: '18px', value: '18px'},
      {label: '24px', value: '24px'},
    ]),
  },
});

editorMaterials.registryMaterial('button', {
  name: '按钮',
  preview: () => <ElButton>按钮</ElButton>,
  element: ({props}) => <ElButton type={props.type} size={props.size}>{props.text || '按钮'}</ElButton>,
  props: {
    text: createInputControl('显示文本'),
    type: createSelectControl('按钮类型', [
      {label: 'primary', value: 'primary'},
      {label: 'success', value: 'success'},
      {label: 'warning', value: 'warning'},
      {label: 'danger', value: 'danger'},
      {label: 'info', value: 'info'},
      {label: 'text', value: 'text'},
    ]),
    size: createSelectControl('按钮尺寸', [
      {label: 'default', value: ''},
      {label: 'medium', value: 'medium'},
      {label: 'small', value: 'small'},
      {label: 'mini', value: 'mini'},
    ]),
  },
});

editorMaterials.registryMaterial('select', {
  name: '选择框',
  preview: () => <ElSelect />,
  element: ({props}) => {
    const options = props.options as {label: string, value: string}[] || [];

    return (
      /*这里加key为了让select响应数据变化*/
      <ElSelect key={options.map((item) => item.value).join(',')}>
        {
          options.map((option, index) => {
            return <ElOption key={index} label={option.label} value={option.value} />;
          })
        }
      </ElSelect>
    );
  },
  props: {
    options: createPickerOptionsControl('下拉选项配置', [
      {label: '显示值', field: 'label'},
      {label: '绑定值', field: 'value'},
      {label: '备注', field: 'comment'},
    ], 'label'),
  },
});

editorMaterials.registryMaterial('input', {
  name: '输入框',
  preview: () => <ElInput readonly/>,
  element: ({model}) => {
    console.log(model);
    return <ElInput {...model.default} />
  },
  model: {
    default: '绑定字段'
  }
});
